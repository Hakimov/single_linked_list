CC=gcc

CFLAGS=-c -Wall

all: listRun

listRun: main.o list.o
	$(CC) main.o list.o -o listRun

main.o: main.c
	$(CC) $(CFLAGS) main.c

list.o: list.c
	$(CC) $(CFLAGS) list.c

clean:
	rm -rf *.o listRun
