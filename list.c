/* Copyright ProsoftSystems Homework. All right not reserved
* Author: Hakimov.A
* 2017
* */

#include "list.h"
#include <stdio.h>

static size_t push_back_call_count  = 0;
static size_t push_front_call_count = 0;
static size_t pop_back_call_count   = 0;
static size_t pop_front_call_count  = 0;
static size_t insert_call_count     = 0;
static size_t set_list_call_count   = 0;
static size_t get_list_call_count   = 0;
static size_t new_list_call_count   = 0;
static size_t size_list_call_count  = 0;
static size_t find_list_call_count  = 0;
static size_t delete_list_call_count= 0;
static size_t init_call_count       = 0;
static size_t malloc_count          = 0;
static size_t free_count            = 0;

size_t get_push_back_call_count()   { return push_back_call_count;  }
size_t get_push_front_call_count()  { return push_front_call_count; }
size_t get_pop_back_call_count()    { return pop_back_call_count;   }
size_t get_pop_front_call_count()   { return pop_front_call_count;  }
size_t get_init_call_count()        { return init_call_count;       }
size_t get_insert_call_count()      { return insert_call_count;     }
size_t get_set_list_call_count()    { return set_list_call_count;   }
size_t get_get_list_call_count()    { return get_list_call_count;   }
size_t get_new_list_call_count()    { return new_list_call_count;   }
size_t get_size_list_call_count()   { return size_list_call_count;  }
size_t get_find_list_call_count()   { return find_list_call_count;  }
size_t get_delete_list_call_count() { return delete_list_call_count;}
size_t get_malloc_count()           { return malloc_count;          }
size_t get_free_count()             { return free_count;            }

void print_all_statistics() {
    log("Statistic of all parameters:\n");
    log("push back call count:%d\n",   get_push_back_call_count());
    log("push front call count:%d\n",  get_push_front_call_count());
    log("pop back call_count:%d\n",    get_pop_back_call_count());
    log("pop front call count:%d\n",   get_pop_front_call_count());
    log("insert call_count:%d\n",      get_insert_call_count());
    log("init call_count:%d\n",        get_init_call_count());
    log("set list call count:%d\n",    get_set_list_call_count());
    log("get list call count:%d\n",    get_get_list_call_count());
    log("new list call count:%d\n",    get_new_list_call_count());
    log("size list call count:%d\n",   get_size_list_call_count());
    log("find list call count:%d\n",   get_find_list_call_count());
    log("delete list call count:%d\n", get_delete_list_call_count());
    log("malloc count:%d\n",           get_malloc_count() );
    log("free count:%d\n",             get_free_count());
}


void show_list(List* list, void (*print)(void*)) {
    List *tmp = list;
    assert(tmp);
    while (tmp != NULL) {
        (*print)(tmp->val);
        tmp = tmp->next;
    }
    printf("\n");
}

void print_int(void *IntList) {
    printf(" %d", *(int*)IntList);
}

void print_float(void *FloatList) {
    printf(" %.2f", *(float*)FloatList);
}

void print_char(void *CharList) {
    printf(" %c", *(char*)CharList);
}

void print_string(void *StringList) {
    printf(" %s", StringList);
}

void print_point(struct Point *point) {
    printf(" x:%.2f y:%.2f z:%.2f \n", point->x, point->y, point->z);
}

/* Функция инициализации списка при объявлении
*/
List* new_list(size_t size) {
    List* tmp = malloc(sizeof(List));
    memcnt(sizeof(List));
    tmp->val = NULL;
    tmp->next = NULL;
    tmp->size = size;

    log("Added node address (new_list) - list:%p, list_val:%p, list_next:%p | ", tmp, tmp->val, tmp->next);
    new_list_call_count++;
    log("new_list_call_count:%d\n", new_list_call_count);

    return tmp;
}

/* Функция инициализации списка определенной размерностью
* и определенными значениями элементов
* Аргументы:
* l: указатель на список
* size: размерность
* val: значение инициализации элементов
*/
void init_list(List** list, const unsigned int size, const void* val) {
    List* l = *list;
    assert(l);
    for (size_t i = 0; i < size; ++i) {
        l->next = malloc(sizeof(List));
        memcnt(sizeof(List));
        assert(l->next);
        l->val = malloc((*list)->size);
        memcnt((*list)->size);
        assert(l->val);
        memcpy(l->val, val, (*list)->size);
        log("Added node address (init_list) - list:%p, list_val:%p, list_next:%p \n", l, l->val, l->next);
        l = l->next;
    }
    init_call_count++;
    log("init_call_count:%d\n", init_call_count);
}

/* Функция добавления элементов в конец списка
* Аргументы:
* list: указатель на список
* val: значение
*/
void push_back(List** list, const void* val) {
    List* tmp = *list;
    assert(tmp);
    if (tmp->val != NULL) {
        while (tmp->next)
            tmp = tmp->next;
        tmp->next = (List*) malloc(sizeof(*list));
        memcnt(sizeof(*list));
        assert(tmp->next);
        tmp = tmp->next;
    }
    tmp->val = malloc((*list)->size);
    assert(tmp->val);
    memcnt((*list)->size);
    memcpy(tmp->val, val, (*list)->size);
    tmp->next = NULL;

    log("Added node address (push_back) - list:%p, list_val:%p, list_next:%p | ", tmp, tmp->val, tmp->next);
    push_back_call_count++;
    log("push_back_call_count:%d \n", push_back_call_count);
}

/* Функция добавления элементов в начало списка
* Аргументы:
* list: указатель на список
* val: значение
*/
void push_front(List** list, const void* val) {
    if ((*list)->val == NULL) {
        (*list)->val = malloc((*list)->size);
        memcnt((*list)->size);
        assert((*list)->val);
        memcpy((*list)->val, val, (*list)->size);
    } else {
        List* tmp_list = new_list((*list)->size);
        assert(tmp_list);
        tmp_list->val = malloc((*list)->size);
        memcnt((*list)->size);
        memcpy(tmp_list->val, val, (*list)->size);
        tmp_list->next = (List*) malloc(sizeof(*list));
        memcnt(sizeof(*list));
        assert(tmp_list->next);
        tmp_list->next = *list;
        *list = tmp_list;

        log("Added node address (push_front) - list:%p, list_val:%p, list_next:%p | ", tmp_list, tmp_list->val, tmp_list->next);
    }

    push_front_call_count++;
    log("push_front_call_count:%d \n", push_front_call_count);
}

/* Функция удаления элементов из конца списка
* Аргументы:
* list: указатель на список
*/
void pop_back(List** list) {
    for (List* l = *list; l != NULL; l = l->next) {
        if (l->next != NULL) {
            if (l->next->next == NULL) {
                log("Added node address (pop_back) - list:%p, list_val:%p, list_next:%p | ", l, l->val, l->next);
                l->next = NULL;
                break;
            }
        } else
            l->val = NULL;
    }

    pop_back_call_count++;
    log("pop_back_call_count:%d\n", pop_back_call_count);
}

/* Функция удаления элементов из начала списка
* Аргументы:
* list: указатель на список
*/
void pop_front(List** list) {
    List* tmp = *list;
    assert(tmp);
    if (tmp->next != NULL)
        tmp = tmp->next;
    *list = tmp;
    log("Deleted node address (pop_front) - list:%p, list_val:%p, list_next:%p | ", tmp, tmp->val, tmp->next);

    pop_front_call_count++;
    log("pop_front_call_count:%d\n", pop_front_call_count);
}

/* Return size of current list
* Функция для определения размера списка
* Аргументы:
* list: указатель на список
*/
unsigned int size_list(const List* list) {

    size_list_call_count++;
    log("size_list_call_count:%d\n", size_list_call_count);

    if (list != NULL && list->val != NULL) {
        int i = 0;
        for (List* l = list; l != NULL; l = l->next)
            ++i;
        return (i - 1);
    } else {
        return 0;
    }
}

/* Функция для получения элемента списка по индексу
* Аргументы:
* list: указатель на список
* index: индекс по которому нужно получить значение
*/
void* get_list(const List** list, const unsigned int index) {

    get_list_call_count++;
    log("get_list_call_count:%d\n", get_list_call_count);

    unsigned int i = 0;
    if ((*list != NULL) && (index <= size_list(*list))) {
        for (List* l = *list; l != NULL; l = l->next, ++i)
            if (i == index)
                return l->val;
    }
    return -1;
}

/* Функция для установки значения элементу списка по индексу
* Аргументы:
* list: указатель на список
* index: индекс по которому нужно установить значение списка
* val: значение
*/
void set_list(List** list, const size_t index, const void* val) {
    assert(*list);
    List* l = *list;
    size_t i = 0;
    while (l->next) {
        if (i == index)
            memcpy(l->val, val, (*list)->size);
        l = l->next;
        log("Setted node adress (set_list) - list:%p, list_val:%p, list_next:%p \n", l, l->val, l->next);
        assert(l);
        ++i;
    }

    set_list_call_count++;
    log("set_list_call_count:%d\n", set_list_call_count);
}

/* Функция поиска элемента списка
* Аргументы:
* list: указатель на список
* val: значение по которому будет производиться поиск
*/
int find_list(const List** list, const void* val) {

    find_list_call_count++;
    log("find_list_call_count:%d | ", find_list_call_count);

    int index = 0;
    if (*list != NULL)
        for (List* l = *list; l != NULL; l = l->next, ++index){
            log("memcmp(val, l->val):%d\n", memcmp(val, l->val, 1));
            if (!memcmp(val, l->val, (*list)->size)) {
                log("Finded node adress (find_list) - list:%p \n", l);
                return index;
            }
        }
    return -1;
}

/* Функция вставки в список элемента между двумя элементами
* Аргументы:
* list: указатель на список
* index: индекс по которому будет вставлено новое значение
* val: значение вставляемого элемента
*/
void insert_list(List** list, const unsigned int index, const void* val) {
    assert(*list);
    List* tmp = new_list((*list)->size);
    assert(tmp);
    tmp->next = (List*)malloc(sizeof(*list));
    memcnt((*list)->size);
    assert(tmp->next);
    tmp->val = malloc((*list)->size);
    memcnt((*list)->size);
    assert(tmp->val);
    log("Added node adress (insert_list) - list:%p, list_val:%p, list_next:%p ", tmp, tmp->val, tmp->next);
    memcpy(tmp->val, val, (*list)->size);
    size_t cnt = 0;
    for (List* l = (*list); l != NULL; l = l->next, ++cnt)
        if (cnt == index) {
            tmp->next = l->next;
            l->next = tmp;
            break;
        }

    insert_call_count++;
    log("insert_call_count:%d \n", insert_call_count);
}

/* Функция для удаления списка (реализована рекурсивно)
*/
void delete_list(List** l) {
    if ((*l)->next != NULL) {
        delete_list(&((*l)->next));
        log("Deleted node adress (delete_list) - list:%p, list_val:%p, list_next:%p | ", (*l), (*l)->val, (*l)->next);
        free((void*)(*l)->val);
        freecnt;
        free((List*)(*l)->next);
        freecnt;
    }

    delete_list_call_count++;
    log("delete_list_call_count:%d\n", delete_list_call_count);
}
