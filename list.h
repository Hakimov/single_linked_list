/* Copyright ProsoftSystems Homework. All right not reserved
 * Author: Hakimov.A
 * 2017
 * */

#ifndef LIST_H_
#define LIST_H_

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "point.h"

#define DEBUG
 
#ifdef DEBUG
    #define log(...)	printf(__VA_ARGS__)
    #define memcnt(size)    malloc_count += size
    #define freecnt         free_count += 1
#else
    #define log(...)
    #define memcnt(size)
    #define freecnt
#endif



typedef struct List {
    void* val;          // void* get use any type
    size_t size;
    struct List* next;
} List;


List* new_list(size_t size);
void init_list(List** list, const unsigned int size, const void* val);
void push_back(List** list, const void* val);
void push_front(List** list, const void* val);
void pop_back(List** list);
void pop_front(List** list);
unsigned int size_list(const List* list);
// int empty(const List* list);
void* get_list(const struct List** list, const unsigned int index);
void set_list(List** list, const size_t index, const void* val);
int find_list(const List** list, const void* val);
void insert_list(List** list, const unsigned int index, const void* val);
void delete_list(List** l);

void print_all_statistics() ;
size_t get_push_back_call_count();
size_t get_push_front_call_count();
size_t get_pop_back_call_count();
size_t get_pop_front_call_count();
size_t get_insert_call_count();
size_t get_set_list_call_count();
size_t get_get_list_call_count();
size_t get_new_list_call_count();
size_t get_size_list_call_count();
size_t get_find_list_call_count();
size_t get_delete_list_call_count();
size_t get_malloc_count();
size_t get_free_count();

void show_list(List* list, void(*print)(void*));
void print_int(void *IntList);
void print_float(void *FloatList);
void print_char(void *FloatList);
void print_string(void *StringList);
void print_point(struct Point *point);


#endif  // LIST_H_
