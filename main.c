/* Copyright ProsoftSystems Homework. All right not reserved
 * Author: Hakimov.A
 * 2017
 * */

#include <assert.h>
#include "list.h"
#include "point.h"


//
void push_test() {
    printf("Push test...\n");
    List *list = new_list(sizeof(float));
    for (float i = 0; i < 10; i++) {
        push_back(&list, &i);
    }
    for (float i = 0; i < 10; i++) {
        push_front(&list, &i);
    }
    show_list(list, print_float);
    delete_list(&list);
    printf("Push test complete\n\n");
}

//
void pop_test() {
    printf("Pop test...\n");
    
    List *list = new_list(sizeof(float));
    for (float i = 0; i < 10; i++) {
        push_back(&list, &i);
    }
    
    show_list(list, print_float);
    
    printf("\n");
    pop_front(&list);
    pop_back(&list);
    
    show_list(list, print_float);
    delete_list(&list);
    printf("Pop test complete\n\n");
}

//
void set_get_test() {
    printf("Set Get test...\n");
    
    List* list = new_list(sizeof(float));
    printf("size = %d\n", size_list(list));

    // init all list with "?"
    float val = 0.2;
    init_list(&list, 10, &val);
    show_list(list, print_float);
    printf("size = %d\n", size_list(list));

    // get and print all list
    for (size_t i = 0; i < 10; i++) {
        printf("get - %.1f\n", *(float*)(get_list(&list, i)));
    }
    printf("\n");
    
    // set all list a-z symbols
    val = 3.3;
    for (int i = 2; i < 5; i++)
        set_list(&list, i, &val);

    // get and print all list
    for (char i = 0; i < 10; i++)
        printf("get - %.2f \n", *(float*)get_list(&list, i));
    
    printf("\nsize = %d\n", size_list(list));
    delete_list(&list);
    printf("Set Get test complete\n\n");
}

//
void insert_test() {
    printf("Insert test...\n");
    List *list = new_list(sizeof(int));
    for (int i = 0; i < 10; i++) {
        push_back(&list, &i);
    }
    show_list(list, print_int);
    printf("\n");
    int var = 73;
    insert_list(&list, 8, &var);
    insert_list(&list, 5, &var);
    insert_list(&list, 0, &var);
    
    // show list for compare with previous result
    show_list(list, print_int);
    delete_list(&list);
    printf("Insert test complete\n\n");
}

//
void find_test() {
    printf("Find test...\n");
    List* list = new_list(sizeof(char));
    // set all list with numbers
    char numb1 = 'f';
    for (char i = 0; i < 12; ++i)
        push_back(&list, &numb1);
    numb1 = 'd';
    push_back(&list, &numb1);
    
    show_list(list, print_char);
    printf("\n");
    // find number in list and print his index
    char numb = 'd';
    printf("index find number %d\n", find_list(&list, &numb));
    delete_list(&list);
    printf("Find test complete\n\n");
}

//
void delete_test() {
    printf("Delete test...\n");
    List* list = new_list(sizeof(int));
    // set all list with numbers
    for (int i = 1; i <= 10; i++)
        push_back(&list, &i);
    printf("size = %d\n", size_list(list));
    // delete list
    delete_list(&list);
    printf("size = %d\n", size_list(list));
    printf("Delete test complete\n\n");
}



void string_test()
{
    printf("String test...\n");
    List *stringList = new_list(sizeof(char*));
    char* str1 = "Hello ";
    char* str2 = "World!";
    push_back(&stringList, str1);
    push_back(&stringList, str2);
    printf("%s\n", get_list(&stringList, 0));
    printf("%s\n", get_list(&stringList, 1));
    show_list(stringList, print_string);
    printf("String test complete\n\n");
}

void struct_test()
{
    printf("Struct test...\n");
    List *structList = new_list(sizeof(struct Point));

    struct Point point_1 = {0, 0, 0};
    struct Point point_2 = {3.1, 7.5, 0.1};
    struct Point point_3 = {4.6, 0.3, 5.9};

    push_back(&structList, &point_1);
    push_back(&structList, &point_2);
    push_back(&structList, &point_3);

    show_list(structList, &print_point);
    printf("Struct test complete\n\n");
}

//
void test() {
    push_test();    // тест добавления узлов в начало и конец списка
    pop_test();     // тест удаления узлов из начала и конца списка
    set_get_test(); // тест изменения и получения значения узла из произвольного места в списке
    insert_test();  // тест добавления узла в произвольное место 
    find_test();    // тест поиска в списке
    delete_test();  // тест удаления списка
    string_test();  // тест использования строк в списке
    struct_test();  // тест использования структуры Point в списке (тестрируется print через callback)

    /* Примечание: для тестирования callback ф-ций для вывода на консоль переменных 
     * с типами данных float, char, int отдельная функция не создавалась, поскольку  
     * их вывод использовался в других тестах
     */
}

int main() {
#if 1
    test();
#endif
    print_all_statistics();
    return 0;
}


